const express = require('express')
const socket = require('socket.io')

const port = 5001

// App setup
const app = express()
const server = app.listen(port, () => {
    console.log(`Server is listening on port ${port}`)
})

// Static files
app.use(express.static('public'))

// Socket setup
const io = socket(server)

io.on("connection", (socket) => {
    console.log(`Made socket connection: ${socket.id}`)

    socket.on('chat', (data) => {
        io.sockets.emit('chat', data)
    })

    socket.on('typing', (data) => {
        socket.broadcast.emit('typing', data)
    })

})